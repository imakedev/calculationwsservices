package th.co.imake.steamtable.rest.resource;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.opencsv.CSVWriter;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.json.JSONObject;
import org.restlet.data.Status;
import org.restlet.ext.json.JsonRepresentation;
import org.restlet.representation.Representation;
import org.restlet.representation.Variant;
import org.restlet.resource.ResourceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import th.co.imake.steamtable.service.CalculationService;

import java.io.*;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.Map;

/**
 * Created by imake on 29/06/2016.
 */
public class DownloadDataResource extends BaseResource {
    private static DecimalFormat df2 = new DecimalFormat(".##");
    //private static String  ROOT_PATH="/Users/imake/PhpstormProjects/git_project/ais_v3/public/exportData/";
    private static String  ROOT_PATH="/var/www/ais_v3/public/exportData/";
    @Autowired
    @Qualifier("calculationService")
    private CalculationService calculationService;

    @Autowired
    private com.thoughtworks.xstream.XStream jsonXstream;

    public DownloadDataResource() {
        super();
        logger.debug("into constructor DownloadDataResource");
        // TODO Auto-generated constructor stub
    }

    @Override
    protected void doInit() throws ResourceException {
        // TODO Auto-generated method stub
        super.doInit();
        logger.debug("into doInit");
    }

    @Override
    protected Representation post(Representation entity, Variant variant)
            throws ResourceException {
        // TODO Auto-generated method stub
        logger.debug("into Post DownloadDataResource ");
        InputStream in = null;
        Reader reader=null;
        Representation rep=null;
        String fileName="";
        try {
            in = entity.getStream();
            //  InputStream input = new URL("http://example.com/foo.json").openStream();
            reader = new InputStreamReader(in, "UTF-8");
            JsonParser parser = new JsonParser();
            JsonObject json = parser.parse(reader).getAsJsonObject();//getAsJsonObject();
            JsonArray jsonData=json.get("mydata").getAsJsonArray();
           // JsonArray jsonData = parser.parse(reader).getAsJsonArray();//getAsJsonObject();
            JsonArray jsonTrends = json.get("trends").getAsJsonArray();
            String downloadType = json.get("downloadType").getAsString();
            int size=jsonData.size();
            if(size>0){
                org.joda.time.DateTime dt1 = new org.joda.time.DateTime(new Date().getTime());

                String monthStr = dt1.getMonthOfYear() + "";
                String yearStr = dt1.getYear() + "";
                int day=dt1.getDayOfMonth();
                String dayStr=day>9?(day+""):("0"+day);
                monthStr = monthStr.length() > 1 ? monthStr : "0" + monthStr;

                String path = ROOT_PATH+"/"+yearStr + "_" + monthStr + "_"+dayStr;

                createDirectoryIfNeeded(path);
                fileName=genToken();
                if(downloadType.equals("xls") || downloadType.equals("ods")){
                    HSSFWorkbook wb = new HSSFWorkbook();
                    HSSFSheet sheet = wb.createSheet("ExportData");

                    int indexRow = 0;
                    HSSFCellStyle cellStyle = wb.createCellStyle();
                    HSSFCellStyle cellStyle2 = wb.createCellStyle();
                    cellStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);
                    cellStyle.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
                    cellStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
                    cellStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
                    cellStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
                    cellStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);

                    cellStyle.setFillBackgroundColor(new HSSFColor.GREY_25_PERCENT().getIndex());
                    cellStyle.setWrapText(true);

                    cellStyle2.setAlignment(HSSFCellStyle.ALIGN_CENTER);
                    cellStyle2.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
                    cellStyle2.setBorderBottom(HSSFCellStyle.BORDER_THIN);
                    cellStyle2.setBorderLeft(HSSFCellStyle.BORDER_THIN);
                    cellStyle2.setBorderRight(HSSFCellStyle.BORDER_THIN);
                    cellStyle2.setBorderTop(HSSFCellStyle.BORDER_THIN);
                    cellStyle2.setWrapText(true);

                    HSSFRow row = sheet.createRow(indexRow);
                    HSSFCell cell =null;
                    JsonElement objFirst = (JsonElement)jsonData.get(0);
                    JsonObject jsonobjFirstObj=objFirst.getAsJsonObject();
                    for(int i=0;i<size;i++){
                        sheet.setColumnWidth((short)i,(short)((25*8)/((double)1/20) ));
                    }
                    int index=0;
                    for (Map.Entry<String,JsonElement> entry : jsonobjFirstObj.entrySet()) {
                        String header="";
                        if(index>0){
                            JsonObject jsonTrendsObject =jsonTrends.get(index-1).getAsJsonObject();
                            header=jsonTrendsObject.get("C").getAsString();
                        }else{
                            header=entry.getKey();
                        }

                        cell = row.createCell((short)index++);

                        cell.setCellStyle(cellStyle);
                        cell.setCellValue(header);
                    }
                    indexRow++;
                    //
                    for (JsonElement jsonElement : jsonData) {
                        JsonObject jsonObj=jsonElement.getAsJsonObject();
                        row = sheet.createRow(indexRow++);
                        index=0;
                        for (Map.Entry<String,JsonElement> entry : jsonObj.entrySet()) {
                            cell = row.createCell((short)index++);

                            if(index>1) {
                                cell.setCellType(Cell.CELL_TYPE_NUMERIC);
                                cell.setCellValue(Double.parseDouble(df2.format(entry.getValue().getAsDouble())));

                            }
                            else
                                cell.setCellValue(entry.getValue().getAsString());
                            cell.setCellStyle(cellStyle2);
                        }
                    }


                    File someFile = new File(path+"/"+fileName+"."+downloadType);
                    FileOutputStream fos=null;
                    try {
                        fos = new FileOutputStream(someFile);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }

                    try {
                        wb.write(fos);
                    } finally {

                        try {
                            wb.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        try {
                            fos.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }


                }else if(downloadType.equals("csv")){
                    //CSVWriter writer = new CSVWriter(new FileWriter(path+"/"+fileName+".csv"), '\t');
                    CSVWriter writer = new CSVWriter(new FileWriter(path+"/"+fileName+".csv"));
                    JsonElement objFirst = (JsonElement)jsonData.get(0);
                    JsonObject jsonobjFirstObj=objFirst.getAsJsonObject();
                    // feed in your array (or convert your data to an array)
                    int sizeEntry=jsonobjFirstObj.entrySet().size();
                    String[] entries_header =  new String[sizeEntry];
                    int index=0;
                    for (Map.Entry<String,JsonElement> entry : jsonobjFirstObj.entrySet()) {
                        String header="";
                        if(index>0){
                            JsonObject jsonTrendsObject =jsonTrends.get(index-1).getAsJsonObject();
                            header=jsonTrendsObject.get("C").getAsString();
                        }else{
                            header=entry.getKey();
                        }
                        header=header.replaceAll(",","");
                        entries_header[index++]=header;
                    }
                    writer.writeNext(entries_header);
                    //
                    for (JsonElement jsonElement : jsonData) {
                        JsonObject jsonObj=jsonElement.getAsJsonObject();
                        String[] entries =  new String[jsonObj.entrySet().size()];
                        index=0;
                        for (Map.Entry<String,JsonElement> entry : jsonObj.entrySet()) {

                            String value="";
                            if(index>1) {
                                value=(Double.parseDouble(df2.format(entry.getValue().getAsDouble())))+"";
                            }
                            else
                                value=entry.getValue().getAsString();
                            entries[index++]=value;
                        }
                        writer.writeNext(entries);
                    }

                    writer.close();
                }
            }




            String jsonStr="{\"fileName\":\""+fileName+"\",\"fileType\":\""+downloadType+"\"}";
            rep = new JsonRepresentation(jsonStr);
            getResponse().setEntity(rep);
            getResponse().setStatus(Status.SUCCESS_OK);

        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally {
            logger.debug(" into Finally Call");
            try {
                if(reader!=null)
                    reader.close();
                if (in != null)
                    in.close();

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return  rep;

    }
    private void createDirectoryIfNeeded(String directoryName) {
        File theDir = new File(directoryName);

        // if the directory does not exist, create it
        if (!theDir.exists()) {
            //boolean cancreate = theDir.mkdir();
            theDir.mkdir();
        }

    }
    private String genToken() {
        StringBuffer sb = new StringBuffer();
        for (int i = 36; i > 0; i -= 12) {
            int n = Math.min(12, Math.abs(i));
            sb.append(org.apache.commons.lang.StringUtils.leftPad(Long.toString(Math.round(Math.random() * Math.pow(36, n)), 36), n, '0'));
        }
        return sb.toString();
    }
}
