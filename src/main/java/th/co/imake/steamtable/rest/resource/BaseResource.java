package th.co.imake.steamtable.rest.resource;

import org.apache.log4j.Logger;
import org.restlet.data.MediaType;
import org.restlet.ext.xml.DomRepresentation;
import org.restlet.representation.Representation;
import org.restlet.representation.Variant;
import org.restlet.resource.ResourceException;
import org.restlet.resource.ServerResource;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;


/**
 * @author Chatchai Pimtun
 */
public abstract class BaseResource extends ServerResource {
    protected static final Logger logger = Logger.getLogger("CalculationServicesLog");
    protected static final String DATE_FORMAT = "yyyy-MM-dd";
    protected String HOST = "";
    protected String PATH_REF = "";

    @Override
    protected void doInit() throws ResourceException {
        // TODO Auto-generated method stub
        super.doInit();
        /*getReference().getHostDomain();
		getReference().getHostPort();
		getReference().getSchemeProtocol().toString().toLowerCase();
		getReference().getPath();*/
        // get host
        HOST = getRequest().getProtocol().toString().toLowerCase() + "://"
                + getRequest().getResourceRef().getHostDomain() + ":"
                + getRequest().getResourceRef().getHostPort();
        // get ResourceRef
        PATH_REF = getRequest().getResourceRef().getPath();
    }


    public BaseResource() {
        super();
        logger.debug("into constructor BaseResource");
        // Allow modifications of this resource via POST requests.
        //setModifiable(true);
        // Declare the kind of representations supported by this resource.
        //	 getVariants().add(new Variant(MediaType.TEXT_HTML));
        getVariants().add(new Variant(MediaType.TEXT_XML));
        getVariants().add(new Variant(MediaType.APPLICATION_ATOM));
        getVariants().add(new Variant(MediaType.APPLICATION_ALL_XML));
        // TODO Auto-generated constructor stub
    }



}
