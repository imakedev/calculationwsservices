package th.co.imake.steamtable.rest.resource;

import com.google.gson.Gson;
import net.objecthunter.exp4j.Expression;
import net.objecthunter.exp4j.ExpressionBuilder;
import org.restlet.data.Status;
import org.restlet.ext.json.JsonRepresentation;
import org.restlet.representation.Representation;
import org.restlet.representation.Variant;
import org.restlet.resource.ResourceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import th.co.imake.steamtable.model.CalculationM;
import th.co.imake.steamtable.model.FormulaM;
import th.co.imake.steamtable.service.CalculationService;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/**
 * Created by imake on 22/11/2015.
 */
public class CalculationResource extends BaseResource {
   // private static final Logger logger = Logger.getLogger(ServiceConstant.LOG_APPENDER);
    @Autowired
    @Qualifier("calculationService")
    private CalculationService calculationService;

    @Autowired
    private com.thoughtworks.xstream.XStream jsonXstream;

    public CalculationResource() {
        super();
        logger.debug("into constructor ChartEntity");
        // TODO Auto-generated constructor stub
    }

    @Override
    protected void doInit() throws ResourceException {
        // TODO Auto-generated method stub
        super.doInit();
        logger.debug("into doInit");
    }

    @Override
    protected Representation post(Representation entity, Variant variant)
            throws ResourceException {
        // TODO Auto-generated method stub
        logger.debug("into Post ConsultantReportResource 2");
        InputStream in = null;
        Reader reader=null;
        Representation rep=null;
        try {
            in = entity.getStream();
            Gson gson =new Gson();
          //  InputStream input = new URL("http://example.com/foo.json").openStream();
            reader = new InputStreamReader(in, "UTF-8");
            CalculationM data = new Gson().fromJson(reader, CalculationM.class);

           List<FormulaM> results= calculationService.calculate(data.getFormula());
            //results.get(0).setResult("result ok");
            //results.get(1).setResult("ทดสอบok");
            data.setFormula(results);

            String jsonStr=gson.toJson(data);
            if(data.getCallBackName()!=null && data.getCallBackName().length()>0){
                jsonStr=data.getCallBackName()+"("+jsonStr+")";
            }
            rep = new JsonRepresentation(jsonStr);
            getResponse().setEntity(rep);
            getResponse().setStatus(Status.SUCCESS_OK);



        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally {
            logger.debug(" into Finally Call");
            try {
                if(reader!=null)
                    reader.close();
                if (in != null)
                    in.close();

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return  rep;

    }
    @Override
    protected Representation get(Variant variant) throws ResourceException {
        // TODO Auto-generated method stutb
        Set<String> names=getQuery().getNames();
        Iterator iter = names.iterator();
        String callBackName=getQuery().getValues("callBackName");
        List<FormulaM> formulas =new ArrayList<FormulaM>();
        int index=1;
        int indexUsed=0;
        while (iter.hasNext()) {
            String formula=(String)iter.next();
            if(formula.startsWith("formula") && formula.indexOf("value")!=-1){
                FormulaM formula1 =new FormulaM();
                String value=getQuery().getValues(formula);
                String key=getQuery().getValues("formula["+indexUsed+"][key]");
                String time=getQuery().getValues("formula["+indexUsed+"][time]");

                formula1.setKey(key);
                formula1.setTime(time);
                formula1.setValue(value);

                formulas.add(formula1);
                indexUsed++;
            }
        }


        CalculationM calculationM=new CalculationM();
      //  List<FormulaM> formulas =new ArrayList<FormulaM>(2);
        /*FormulaM formula1 =new FormulaM();
        formula1.setKey("1");
        formula1.setValue("(100/3)*20");

        FormulaM formula2 =new FormulaM();
        formula2.setKey("2");
        formula2.setValue("(300/3)*20");

        formulas.add(formula1);
        formulas.add(formula2);
        */


        List<FormulaM> results= calculationService.calculate(formulas);
        //results.get(0).setResult("result ok");
        //results.get(1).setResult("ทดสอบok");
        calculationM.setFormula(results);

        Gson gson=new Gson();
        String json=gson.toJson(calculationM);
        json=callBackName+"("+json+")";
        Representation rep = new JsonRepresentation(json);
        getResponse().setEntity(rep);
        getResponse().setStatus(Status.SUCCESS_OK);
        /*
        Expression e = new ExpressionBuilder("3 * sin(y) - 2 / (x - 2)")
                .variables("x", "y")
                .build()
                .setVariable("x", 2.3)
                .setVariable("y", 3.14);
        double result = e.evaluate();
        */
/*
        Interpreter interpreter = new Interpreter();
        try {
            interpreter.eval("result = 3 * sin(3.14) - 2 / (2.3 - 2)");
        } catch (EvalError evalError) {
            evalError.printStackTrace();
        }
        try {
        } catch (EvalError evalError) {
            evalError.printStackTrace();
        }
        */
        return rep;

    }


}
