package th.co.imake.steamtable.rest.resource;

import com.google.gson.Gson;
import net.objecthunter.exp4j.Expression;
import net.objecthunter.exp4j.ExpressionBuilder;
import org.restlet.data.Status;
import org.restlet.ext.json.JsonRepresentation;
import org.restlet.representation.Representation;
import org.restlet.representation.Variant;
import org.restlet.resource.ResourceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import th.co.imake.steamtable.model.CalculationM;
import th.co.imake.steamtable.model.ExtractM;
import th.co.imake.steamtable.model.FormulaM;
import th.co.imake.steamtable.model.PatternM;
import th.co.imake.steamtable.service.CalculationService;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by imake on 12/02/2016.
 */
public class ExtractRegularResource extends BaseResource {
    // private static final Logger logger = Logger.getLogger(ServiceConstant.LOG_APPENDER);
    @Autowired
    @Qualifier("calculationService")
    private CalculationService calculationService;

    @Autowired
    private com.thoughtworks.xstream.XStream jsonXstream;

    public ExtractRegularResource() {
        super();
        logger.debug("into constructor ChartEntity");
        // TODO Auto-generated constructor stub
    }

    @Override
    protected void doInit() throws ResourceException {
        // TODO Auto-generated method stub
        super.doInit();
        logger.debug("into doInit");
    }

    @Override
    protected Representation post(Representation entity, Variant variant)
            throws ResourceException {
        // TODO Auto-generated method stub
        logger.debug("into Post ConsultantReportResource 2");
        InputStream in = null;
        Reader reader=null;
        Representation rep=null;
        try {
            in = entity.getStream();
            Gson gson =new Gson();
            //  InputStream input = new URL("http://example.com/foo.json").openStream();
            reader = new InputStreamReader(in, "UTF-8");
            ExtractM data = new Gson().fromJson(reader, ExtractM.class);

            System.out.println(data);
            PatternM results= calculationService.listPattern(data.getPatternM());
            //results.get(0).setResult("result ok");
            //results.get(1).setResult("ทดสอบok");
            data.setPatternM(results);
            String jsonStr=gson.toJson(data);
            if(data.getCallBackName()!=null && data.getCallBackName().length()>0){
                jsonStr=data.getCallBackName()+"("+jsonStr+")";
            }

            rep = new JsonRepresentation(jsonStr);
            getResponse().setEntity(rep);
            getResponse().setStatus(Status.SUCCESS_OK);



        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally {
            logger.debug(" into Finally Call");
            try {
                if(reader!=null)
                    reader.close();
                if (in != null)
                    in.close();

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return  rep;

    }
    @Override
    protected Representation get(Variant variant) throws ResourceException {
        // TODO Auto-generated method stub
        ExtractM extractM=new ExtractM();
        PatternM pattern=new PatternM();
        pattern.setValue("sin(U04D122+U04D123*5)");
        pattern.setPattern("(U[0-9]{1,2})(D[0-9]{1,4})");

        pattern=calculationService.listPattern(pattern);

        extractM.setPatternM(pattern);
        Gson gson=new Gson();
        String json=gson.toJson(extractM);
        json="localJsonpCallback("+json+")";
        System.out.println(json);
        Representation rep = new JsonRepresentation(json);
        getResponse().setEntity(rep);
        getResponse().setStatus(Status.SUCCESS_OK);
        return rep;

    }

}
