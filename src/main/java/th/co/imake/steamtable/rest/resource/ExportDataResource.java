package th.co.imake.steamtable.rest.resource;

import com.google.gson.Gson;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.CellStyle;
import org.restlet.data.CharacterSet;
import org.restlet.data.Disposition;
import org.restlet.data.MediaType;
import org.restlet.data.Status;
import org.restlet.ext.json.JsonRepresentation;
import org.restlet.representation.ByteArrayRepresentation;
import org.restlet.representation.FileRepresentation;
import org.restlet.representation.Representation;
import org.restlet.representation.Variant;
import org.restlet.resource.ResourceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import th.co.imake.steamtable.model.CalculationM;
import th.co.imake.steamtable.model.FormulaM;
import th.co.imake.steamtable.service.CalculationService;

import javax.servlet.ServletOutputStream;
import java.io.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/**
 * Created by imake on 21/04/2016.
 */
public class ExportDataResource extends BaseResource {

    @Autowired
    @Qualifier("calculationService")
    private CalculationService calculationService;

    @Autowired
    private com.thoughtworks.xstream.XStream jsonXstream;

    public ExportDataResource() {
        super();
        logger.debug("into constructor ChartEntity");
        // TODO Auto-generated constructor stub
    }

    @Override
    protected void doInit() throws ResourceException {
        // TODO Auto-generated method stub
        super.doInit();
        logger.debug("into doInit");
    }

    @Override
    protected Representation post(Representation entity, Variant variant)
            throws ResourceException {
        // TODO Auto-generated method stub
        logger.debug("into Post ExportDataResource 2");
        InputStream in = null;
        Reader reader=null;
        Representation rep=null;
        try {
            in = entity.getStream();
            Gson gson =new Gson();
            //  InputStream input = new URL("http://example.com/foo.json").openStream();
            reader = new InputStreamReader(in, "UTF-8");
            CalculationM data = new Gson().fromJson(reader, CalculationM.class);

            // System.out.println(data);
            List<FormulaM> results= calculationService.calculate(data.getFormula());
            //results.get(0).setResult("result ok");
            //results.get(1).setResult("ทดสอบok");
            data.setFormula(results);

            String jsonStr=gson.toJson(data);
            if(data.getCallBackName()!=null && data.getCallBackName().length()>0){
                jsonStr=data.getCallBackName()+"("+jsonStr+")";
            }
            rep = new JsonRepresentation(jsonStr);
            getResponse().setEntity(rep);
            getResponse().setStatus(Status.SUCCESS_OK);



        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally {
            logger.debug(" into Finally Call");
            try {
                if(reader!=null)
                    reader.close();
                if (in != null)
                    in.close();

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return  rep;

    }
    //@Override
    protected Representation get3(Variant variant) throws ResourceException {
        // TODO Auto-generated method stutb
        InputStream in = null;
        Representation rep=null;
        try {
            String url="http://localhost:8080/direct/site.json?sakai.session=030214a4-a320-468a-b406-f70b12232b66";
            HttpClient client = HttpClientBuilder.create().build();
            //HttpPost post=new HttpPost(url);
            HttpGet post=new HttpGet(url);
            HttpResponse response = null;
            try {
                response = client.execute(post);
            } catch (IOException e) {
                e.printStackTrace();
            }
            int statusCode = response.getStatusLine().getStatusCode();
            try {
                in = response.getEntity().getContent();
            } catch (IOException e) {
                e.printStackTrace();
            }

            String jsonStr = null;
            try {
                jsonStr = IOUtils.toString(in, "UTF-8");
            } catch (IOException e) {
                e.printStackTrace();
            }
            System.out.println(jsonStr+"xx");
            //if(data.getCallBackName()!=null && data.getCallBackName().length()>0){
                jsonStr="callBack"+"("+jsonStr+")";
            //}
            rep = new JsonRepresentation(jsonStr);
            getResponse().setEntity(rep);
            getResponse().setStatus(Status.SUCCESS_OK);

        } finally {
            logger.debug(" into Finally Call");
            try {
                if (in != null)
                    in.close();

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return rep;
        //return null;
    }
    @Override
    protected Representation get(Variant variant) throws ResourceException {
        // TODO Auto-generated method stutb
        Representation rep=null;


        // vresultMessage.getResultListObj()
        HSSFWorkbook wb = new HSSFWorkbook();
        HSSFSheet sheet = wb.createSheet("Candidate");
        sheet.protectSheet("015482543");
        //  HSSFRow row = sheet.createRow(0);
        //  HSSFCellStyle style = wb.createCellStyle();

        int indexRow = 0;
        HSSFCellStyle cellStyle = wb.createCellStyle();
        HSSFCellStyle cellStyle2 = wb.createCellStyle();
        cellStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);
        cellStyle.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
        cellStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        cellStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        cellStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
        cellStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);

        cellStyle.setFillBackgroundColor(new HSSFColor.GREY_25_PERCENT().getIndex());
        cellStyle.setWrapText(true);

        cellStyle2.setAlignment(HSSFCellStyle.ALIGN_CENTER);
        cellStyle2.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
        cellStyle2.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        cellStyle2.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        cellStyle2.setBorderRight(HSSFCellStyle.BORDER_THIN);
        cellStyle2.setBorderTop(HSSFCellStyle.BORDER_THIN);
        cellStyle2.setWrapText(true);


        //Header 5
        HSSFRow row = sheet.createRow(indexRow);
        HSSFCell cell = row.createCell((short)0);

        cell = row.createCell((short)0);
        cell.setCellValue("No");
        cell.setCellStyle(cellStyle);

        cell = row.createCell((short)1);
        cell.setCellValue("MCA_USERNAME");
        cell.setCellStyle(cellStyle);
        /*
                cell = row.createCell((short)2);
                cell.setCellValue("Username/Password");
                cell.setCellStyle(cellStyle);


                cell = row.createCell((short)3);
			    cell.setCellValue("Company");
			    cell.setCellStyle(cellStyle);

			    cell = row.createCell((short)4);
			    cell.setCellValue("Series");
			    cell.setCellStyle(cellStyle);
        */
        cell = row.createCell((short)2);
        cell.setCellValue("MCA_FIRST_NAME");
        cell.setCellStyle(cellStyle);

        cell = row.createCell((short)3);
        cell.setCellValue("MCA_LAST_NAME");
        cell.setCellStyle(cellStyle);

        cell = row.createCell((short)4);
        cell.setCellValue("MCA_DEPARTMENT");
        cell.setCellStyle(cellStyle);

        cell = row.createCell((short)5);
        cell.setCellValue("MCA_POSITION");
        cell.setCellStyle(cellStyle);

        cell = row.createCell((short)6);
        cell.setCellValue("MCA_EMAIL");
        cell.setCellStyle(cellStyle);

        cell = row.createCell((short)7);
        cell.setCellValue("MCA_CITIZEN_ID");
        cell.setCellStyle(cellStyle);
        indexRow++;
			   /*
			    sheet.setColumnWidth((short)0,(short)((50*8)/((double)1/20) ));
			    sheet.setColumnWidth((short)1,(short)((50*8)/((double)1/20) ));
			    sheet.setColumnWidth((short)2,(short)((50*8)/((double)1/20) ));
			    sheet.setColumnWidth((short)3,(short)((50*8)/((double)1/20) ));
			    */
        for(int i=0;i<8;i++){
            sheet.setColumnWidth((short)i,(short)((50*8)/((double)1/20) ));
        }
        CellStyle unlockedCellStyle = wb.createCellStyle();
        unlockedCellStyle.setLocked(false);
        int rowIndex=1;
        row = sheet.createRow(indexRow);
        indexRow++;
        cell = row.createCell((short)0);
        cell.setCellValue(rowIndex++);
        cell.setCellStyle(cellStyle2);
        cell = row.createCell((short)1);
        cell.setCellValue( " ");
        cell.setCellStyle(cellStyle2);
                    /*
                   cell = row.createCell((short)2);
                   cell.setCellValue(missCandidate2.getMcaUsername()+" / "+missCandidate2.getMcaPassword());
                   cell.setCellStyle(cellStyle2);

                   cell = row.createCell((short)3);
				     cell.setCellValue(missCandidate2.getMissAccount().getMaName());
				    cell.setCellStyle(cellStyle2);

				    cell = row.createCell((short)4);
				    cell.setCellValue(missCandidate2.getMissSery().getMsSeriesName());
				    cell.setCellStyle(cellStyle2);
                    */
        cell = row.createCell((short)2);
        cell.setCellValue( " ");
        cell.setCellStyle(cellStyle2);
        cell.setCellStyle(unlockedCellStyle);

        cell = row.createCell((short)3);
        cell.setCellValue( " ");
        cell.setCellStyle(cellStyle2);
        cell.setCellStyle(unlockedCellStyle);

        cell = row.createCell((short)4);
        cell.setCellValue( " ");
        cell.setCellStyle(cellStyle2);
        cell.setCellStyle(unlockedCellStyle);

        cell = row.createCell((short)5);
        cell.setCellValue( " ");
        cell.setCellStyle(cellStyle2);
        cell.setCellStyle(unlockedCellStyle);

        cell = row.createCell((short)6);
        cell.setCellValue( " ");
        cell.setCellStyle(cellStyle2);
        cell.setCellStyle(unlockedCellStyle);

        cell = row.createCell((short)7);
        cell.setCellValue( " ");
        cell.setCellStyle(cellStyle2);
        cell.setCellStyle(unlockedCellStyle);

        //response.setHeader("Content-Type", "application/octet-stream; charset=UTF-8");
        //response.setHeader("Content-disposition", "attachment; filename=Candidate.xls");
        // ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        //  byte[] fileContent = outputStream.toByteArray();
       /*
        File someFile = new File("java2.xls");
        FileOutputStream fos=null;
        try {
            fos = new FileOutputStream(someFile);
            //fos.write();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        */
        try {
            // wb.getBytes();
            //  wb.write(fos);
            // rep=new FileRepresentation(new File(wb.getBytes()), MediaType.APPLICATION_OCTET_STREAM);
            rep=new ByteArrayRepresentation(wb.getBytes(), MediaType.APPLICATION_OCTET_STREAM);
            rep.setCharacterSet(CharacterSet.UTF_8);
            Disposition disposition=new Disposition();

            disposition.setType(Disposition.TYPE_ATTACHMENT);
            disposition.setFilename("aoe.xls");
           // rep.setDisposition(disposition);


            // //   rep.getDisposition().setType(Disposition.TYPE_ATTACHMENT);
            // // rep.getDisposition().setFilename("aoe.xls");

        } finally {

            try {
                wb.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            /*
            try {
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            */
            // if(someFile.exists())
            //    someFile.delete();
        }

       /*
        ServletOutputStream servletOutputStream = null;
        try
        {
            servletOutputStream = response.getOutputStream();
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }
        try
        {
            wb.write(servletOutputStream);
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }
        try
        {
            servletOutputStream.flush();
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }
        try
        {
            servletOutputStream.close();
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }
        */
        return rep;
        //return null;
    }


}

