package th.co.imake.steamtable.model;

import java.io.Serializable;

/**
 * Created by imake on 12/02/2016.
 */
public class FormatM implements Serializable {
    private String callBackName;

    public String getCallBackName() {
        return callBackName;
    }

    public void setCallBackName(String callBackName) {
        this.callBackName = callBackName;
    }
}
