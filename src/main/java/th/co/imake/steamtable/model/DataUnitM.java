package th.co.imake.steamtable.model;

import java.io.Serializable;

/**
 * Created by imake on 12/02/2016.
 */
public class DataUnitM implements Serializable {
    private String unitNumber;
    private String dataColumn;
    private String fullUnit;

    public DataUnitM(String fullUnit, String unitNumber, String dataColumn) {
        this.unitNumber = unitNumber;
        this.dataColumn = dataColumn;
        this.fullUnit = fullUnit;
    }


    public String getUnitNumber() {
        return unitNumber;
    }

    public void setUnitNumber(String unitNumber) {
        this.unitNumber = unitNumber;
    }

    public String getDataColumn() {
        return dataColumn;
    }

    public void setDataColumn(String dataColumn) {
        this.dataColumn = dataColumn;
    }

    public String getFullUnit() {
        return fullUnit;
    }

    public void setFullUnit(String fullUnit) {
        this.fullUnit = fullUnit;
    }
}
