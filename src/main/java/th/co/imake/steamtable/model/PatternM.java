package th.co.imake.steamtable.model;

import java.io.Serializable;
import java.util.List;

/**
 * Created by imake on 12/02/2016.
 */
public class PatternM  implements Serializable {
    private String pattern;
    private String value;
    private List<String> results;
    private List<DataUnitM> dataUnits;
    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getPattern() {
        return pattern;
    }

    public void setPattern(String pattern) {
        this.pattern = pattern;
    }

    public List<String> getResults() {
        return results;
    }

    public List<DataUnitM> getDataUnits() {
        return dataUnits;
    }

    public void setDataUnits(List<DataUnitM> dataUnits) {
        this.dataUnits = dataUnits;
    }

    public void setResults(List<String> results) {
        this.results = results;
    }
}
