package th.co.imake.steamtable.service.impl;

import com.hummeling.if97.IF97;
import net.objecthunter.exp4j.Expression;
import net.objecthunter.exp4j.ExpressionBuilder;
import net.objecthunter.exp4j.ValidationResult;
import org.springframework.stereotype.Service;
import th.co.imake.steamtable.model.DataUnitM;
import th.co.imake.steamtable.model.FormulaM;
import th.co.imake.steamtable.model.PatternM;
import th.co.imake.steamtable.service.CalculationService;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

@Service("calculationService")
public class CalculationServiceImpl implements CalculationService {

    IF97 if97 = new IF97(IF97.UnitSystem.ENGINEERING);
    private net.objecthunter.exp4j.function.Function asinh = new net.objecthunter.exp4j.function.Function("asinh", 1) {
       /*
        @Override
        public double apply(double... args) {
            return Math.log(args[0]) / Math.log(args[1]);
        }
        */
       @Override
       public double apply(double... args) {
           return  org.apache.commons.math3.util.FastMath.asinh(args[0]);
       }

    };

    private net.objecthunter.exp4j.function.Function acosh = new net.objecthunter.exp4j.function.Function("acosh", 1) {
        @Override
        public double apply(double... args) {
            return  org.apache.commons.math3.util.FastMath.acosh(args[0]);
        }

    };
    private net.objecthunter.exp4j.function.Function atanh = new net.objecthunter.exp4j.function.Function("atanh", 1) {
        @Override
        public double apply(double... args) {
            return  0.5*Math.log( (args[0] + 1.0) / (args[0] - 1.0));
        }

    };
    //  Enthalpy(t,p)
    private net.objecthunter.exp4j.function.Function enthalpy = new net.objecthunter.exp4j.function.Function("enthalpy", 2) {
        @Override
        public double apply(double... args) {
            // specificEnthalpyPT(p;t)
            return  if97.specificEnthalpyPT(args[1],args[0]);
        }

    };

    // Enthalpy_s_p(s,p)
    private net.objecthunter.exp4j.function.Function enthalpy_s_p = new net.objecthunter.exp4j.function.Function("enthalpy_s_p", 2) {
        @Override
        public double apply(double... args) {
            // specificEnthalpyPT(p;t)
            return  if97.specificEnthalpyPS(args[1],args[0]);
        }

    };

    // Enthalpy_Tsat(t;x)
    private net.objecthunter.exp4j.function.Function enthalpy_tsat = new net.objecthunter.exp4j.function.Function("enthalpy_tsat", 2) {
        @Override
        public double apply(double... args) {
            // specificEnthalpyPT(p;t)
            return  if97.specificEnthalpyTX(args[0],args[1]);
        }

    };

    // Enthalpy_Psat(p;x)
    private net.objecthunter.exp4j.function.Function enthalpy_psat = new net.objecthunter.exp4j.function.Function("enthalpy_psat", 2) {
        @Override
        public double apply(double... args) {
            // specificEnthalpyPT(p;t)
            return  if97.specificEnthalpyPX(args[0],args[1]);
        }

    };

    // Entropy(t;p)
    private net.objecthunter.exp4j.function.Function entropy = new net.objecthunter.exp4j.function.Function("entropy", 2) {
        @Override
        public double apply(double... args) {
            // specificEnthalpyPT(p;t)
            return  if97.specificEntropyPT(args[1],args[0]);
        }

    };

    // Entropy_Tsat(t;x)
    private net.objecthunter.exp4j.function.Function entropy_tsat = new net.objecthunter.exp4j.function.Function("entropy_tsat", 2) {
        @Override
        public double apply(double... args) {
            // specificEnthalpyPT(p;t)
            return  if97.specificEntropyTX(args[0],args[1]);
        }

    };

    // Entropy_Psat(p;x)
    private net.objecthunter.exp4j.function.Function entropy_psat = new net.objecthunter.exp4j.function.Function("entropy_psat", 2) {
        @Override
        public double apply(double... args) {
            // specificEnthalpyPT(p;t)
            return  if97.specificEntropyPX(args[0],args[1]);
        }

    };

    //Density(t;p)
    private net.objecthunter.exp4j.function.Function density = new net.objecthunter.exp4j.function.Function("density", 2) {
        @Override
        public double apply(double... args) {
            // specificEnthalpyPT(p;t)
            return  if97.densityPT(args[1],args[0]);
        }

    };

    //viscosity(t;p)
    private net.objecthunter.exp4j.function.Function viscosity = new net.objecthunter.exp4j.function.Function("viscosity", 2) {
        @Override
        public double apply(double... args) {
            // specificEnthalpyPT(p;t)
            return  if97.dynamicViscosityPT(args[1],args[0]);
        }

    };

    //Density_Tsat(t;x)
    private net.objecthunter.exp4j.function.Function density_tsat = new net.objecthunter.exp4j.function.Function("density_tsat", 2) {
        @Override
        public double apply(double... args) {
            // specificEnthalpyPT(p;t)
            return  if97.densityTX(args[0],args[1]);
        }

    };

    //Density_Psat(p;x)
    private net.objecthunter.exp4j.function.Function density_psat = new net.objecthunter.exp4j.function.Function("density_psat", 2) {
        @Override
        public double apply(double... args) {
            // specificEnthalpyPT(p;t)
            return  if97.densityPX(args[0],args[1]);
        }

    };

    //Temperature_Psat(p)
    private net.objecthunter.exp4j.function.Function temperature_psat = new net.objecthunter.exp4j.function.Function("temperature_psat", 1) {
        @Override
        public double apply(double... args) {
            // specificEnthalpyPT(p;t)
            return  if97.saturationTemperatureP(args[0]);
        }

    };

    //Pressure_Tsat(t);
    private net.objecthunter.exp4j.function.Function pressure_tsat = new net.objecthunter.exp4j.function.Function("pressure_tsat", 1) {
    @Override
        public double apply(double... args) {
            // specificEnthalpyPT(p;t)
            return  if97.saturationPressureT(args[0]);
        }

    };
    //signum(x);
    private net.objecthunter.exp4j.function.Function signum = new net.objecthunter.exp4j.function.Function("signum", 1) {
        @Override
        public double apply(double... args) {
            // specificEnthalpyPT(p;t)
            return  org.apache.commons.math3.util.FastMath.signum(args[0]);
        }

    };

    //mod(x%y);
    private net.objecthunter.exp4j.function.Function mod = new net.objecthunter.exp4j.function.Function("mod", 2) {
        @Override
        public double apply(double... args) {
            // specificEnthalpyPT(p;t)
            return  args[0]%args[1];
        }

    };

    //mod(x%y);
    private net.objecthunter.exp4j.function.Function random = new net.objecthunter.exp4j.function.Function("random", 0) {
        @Override
        public double apply(double... args) {
            // specificEnthalpyPT(p;t)
            return Math.random();
        }

    };
    //decimal(x);
    private net.objecthunter.exp4j.function.Function decimal = new net.objecthunter.exp4j.function.Function("decimal", 1) {
        @Override
        public double apply(double... args) {
            // specificEnthalpyPT(p;t)
            NumberFormat formatter = new DecimalFormat("###.##");

            return Double.valueOf(formatter.format(args[0]));
        }

    };
    //int(x);
    private net.objecthunter.exp4j.function.Function to_int = new net.objecthunter.exp4j.function.Function("int", 1) {
        @Override
        public double apply(double... args) {
            return Double.valueOf(args[0]).intValue();
        }

    };

    List<net.objecthunter.exp4j.function.Function> functions_extends = new ArrayList<net.objecthunter.exp4j.function.Function>();
    static {
      //  functions_extends.add(ad)

    }
    public CalculationServiceImpl() {
        functions_extends.add(asinh);
        functions_extends.add(acosh);
        functions_extends.add(atanh);
        functions_extends.add(enthalpy);
        functions_extends.add(enthalpy_s_p);

        functions_extends.add(enthalpy_tsat);
        functions_extends.add(enthalpy_psat);
        functions_extends.add(entropy);
        functions_extends.add(entropy_tsat);
        functions_extends.add(entropy_psat);
        functions_extends.add(density);
        functions_extends.add(viscosity);
        functions_extends.add(density_tsat);
        functions_extends.add(density_psat);
        functions_extends.add(temperature_psat);
        functions_extends.add(pressure_tsat);
        functions_extends.add(signum);
        functions_extends.add(mod);
        functions_extends.add(random);
        functions_extends.add(decimal);
        functions_extends.add(to_int);
    }
    @Override
    public List<FormulaM> calculate(List<FormulaM> formulas) {
        for (FormulaM formula:formulas) {
            try {
                Expression e = new ExpressionBuilder(formula.getValue()).functions(functions_extends).build();
                ValidationResult res = e.validate();
                System.out.println("formula validate [" + res.isValid() + "]");
                double result = e.evaluate();
                formula.setStatus("OK");
                formula.setResult(String.valueOf(result));
            }catch (Exception e){
                formula.setStatus("ERROR");
                formula.setResult(e.getMessage());
            }
        }
        /*
        double x=3.0;
       double result= Math.log(x + Math.sqrt(x*x + 1.0));
        System.out.println(result);
        // asinh(0.7)","acosh(0.1)","atanh(0.2)"
        System.out.println(org.apache.commons.math3.util.FastMath.asinh(x));
        System.out.println(Math.log(x + Math.sqrt(x*x - 1.0)));
        System.out.println(org.apache.commons.math3.util.FastMath.acosh(x));
        double x2=-2.0;
        System.out.println(org.apache.commons.math3.util.FastMath.atanh(x2));
        //ATanH = Log((1 / value + 1) / (1 / value - 1)) / 2
        result=0.5*Math.log( (x2 + 1.0) / (x2 - 1.0));
        System.out.println(result);
        //org.apache.commons.math3.analysis.function.Asinh().e
        */
        /*
        double result = new net.objecthunter.exp4j.ExpressionBuilder("logb(8, 2)")
                .function(logb)
                .build()
                .evaluate();
                */
        return formulas;
    }

    @Override
    public String eval(String formula) {

        Expression e = new ExpressionBuilder(formula) .build();
                /*.variables("x", "y")
                .build()
                .setVariable("x", 2.3)
                .setVariable("y", 3.14);
                */
        double result = e.evaluate();
        return String.valueOf(result);
        //System.out.println(result);
    }

    @Override
    public PatternM listPattern(PatternM pattern) {

        java.util.regex.Pattern r = java.util.regex.Pattern.compile(pattern.getPattern());

        // Now create matcher object.
        java.util.regex.Matcher m = r.matcher(pattern.getValue());
        List<String> results=new ArrayList<String>();
        List<DataUnitM> dataUnits=new ArrayList<DataUnitM>();

        int count = 0;
        while(m.find()) {
            String fullUnit=m.group(0);
            String unitNumber=m.group(1);
            String dataColumn=m.group(2);
            DataUnitM dataUnitM=new DataUnitM(fullUnit,unitNumber,dataColumn);
            results.add(fullUnit);
            dataUnits.add(dataUnitM);
            //System.out.println("Match number "+count+", value["+ m.group(0)+"]");
            count++;
            //System.out.println("start(): "+m.start());
            //System.out.println("end(): "+m.end());
        }
        pattern.setResults(results);
        pattern.setDataUnits(dataUnits);
        return pattern;
    }
}
