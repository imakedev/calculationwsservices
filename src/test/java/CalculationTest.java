import com.google.gson.Gson;
import com.hummeling.if97.IF97;
import junit.framework.Assert;
import org.apache.commons.io.IOUtils;
import org.mariuszgromada.math.mxparser.Argument;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import th.co.imake.steamtable.model.FormulaM;
import th.co.imake.steamtable.model.PatternM;
import th.co.imake.steamtable.service.CalculationService;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by imake on 03/02/2016.
 */
public class CalculationTest {
    private ClassPathXmlApplicationContext springContext;
    private CalculationService calculationService;
    @BeforeTest
    public void setUp() {
        System.out.println("setUp");
        springContext = new ClassPathXmlApplicationContext(
                new String[] {
                        "config/applicationContext-common.xml"});
        calculationService =(CalculationService)springContext.getBean("calculationService");
    }
    @AfterTest
    public void tearDown() {
        System.out.println("tearDownp");
        springContext.close();
    }
    @Test
    public void testEval() {
        String[] labels={"cos(atan(20/10))"};

        System.out.println(calculationService.eval(labels[0]));

        Assert.assertEquals("Hello World 2", "Hello World 2");

    }
    @Test
    public void testCal() {
        /*
        <option value="Enthalpy(t;p)">Enthalpy(t;p)</option>
                                               <option value="Enthalpy_s_p(s;p)">Enthalpy_s_p(s;p)</option>
                                              <option value="Enthalpy_Tsat(t;x)">Enthalpy_Tsat(t;x)</option>
                                               <option value="Enthalpy_Psat(p;x)">Enthalpy_Psat(p;x)</option>
                                              <option value="Entropy(t;p)">Entropy(t;p)</option>
                                              <option value="Entropy_Tsat(t;x)">Entropy_Tsat(t;x)</option>
                                               <option value="Entropy_Psat(p;x)">Entropy_Psat(t;p)</option>
                                              <option value="Density(t;p)">Density(t;p)</option>
                                               <option value="Viscosity(t;p)">Viscosity(t;p)</option>
                                              <option value="Density_Tsat(t;x)">Density_Tsat(t;x)</option>
                                               <option value="Density_Psat(p;x)">Density_Psat(p;x)</option>
                                              <option value="Temperature_Psat(p)">Temperature_Psat(p)</option>
                                               <option value="Pressure_Tsat(t)">Pressure_Tsat(t)</option>
         */
       // String[] functions={"sin(30)","cos(20)","tan(20)","asin(0.5)","acos(0.5)","atan(0.7)","sinh(0.5)","cosh(0.5)","tanh(0.5)","asinh(0.7)","acosh(3.0)","atanh(-2.0)"};
        String xxx="5*10^-6*(pressure_tsat(273+(20+50)/2+((0.208*(50/150)^2+0.454*(50/150)+0.336)*10.9)/(1-exp(-10.9/((4180*4.2*10.9)/(8586*(3.62*(0.7586+0.0135*((20+50)/2+10.9/2)-0.0001*((20+50)/2+10.9/2)^2)*0.93*0.85)))))))^2-8*10^-5*(pressure_tsat(273+(20+50)/2+((0.208*(50/150)^2+0.454*(50/150)+0.336)*10.9)/(1-exp(-10.9/((4180*4.2*10.9)/(8586*(3.62*(0.7586+0.0135*((20+50)/2+10.9/2)-0.0001*((20+50)/2+10.9/2)^2)*0.93*0.85)))))))+0.974";
       // System.out.println(org.apache.commons.math3.util.FastMath.modl(0));
        String[] functions={"decimal(45313.319831398813)","int(8.2)"};
        System.out.println(Double.valueOf("-99.8").intValue());
        //System.out.println(Number.parseInt("2.3"));
        //"asinh(0.7)","acosh(0.1)","atanh(0.2)"
        List<FormulaM> formulas =new ArrayList<FormulaM>(functions.length);
        for (int i = 0; i < functions.length; i++) {
            FormulaM formula1=new FormulaM();
            formula1.setKey(i+"");
            formula1.setValue(functions[i]);
            formulas.add(formula1);
        }
        /*
        String[] labels={"cos(atan(20/10))"};
        List<FormulaM> formulas =new ArrayList<FormulaM>(2);
        FormulaM formula1=new FormulaM();
        formula1.setKey("1");
        formula1.setValue("cos(atan(20/10))");

        FormulaM formula2=new FormulaM();
        formula2.setKey("2");
        formula2.setValue("3 * sin(3.14) - 2 / (2.3 - 2)");

        formulas.add(formula1);
        formulas.add(formula2);
        */
        List<FormulaM> formula_results=calculationService.calculate(formulas);
        int x=0;
        for (FormulaM formula:formula_results) {
            System.out.println("["+(x++)+"]"+formula.getResult());
        }


        Assert.assertEquals("Hello World 2", "Hello World 2");

    }
    @Test
    public  void testHummeling(){

    }
    // best
    @Test
    public void testUserDefineExp4j(){
       final IF97 if97 = new IF97(IF97.UnitSystem.ENGINEERING);
        net.objecthunter.exp4j.function.Function logb = new net.objecthunter.exp4j.function.Function("logb", 2) {
            @Override
            public double apply(double... args) {
                return Math.log(args[0]) / Math.log(args[1]);
            }
        };
        net.objecthunter.exp4j.function.Function enthalpy_psat = new net.objecthunter.exp4j.function.Function("enthalpy_psat", 2) {
            @Override
            public double apply(double... args) {
                // specificEnthalpyPT(p;t)
                return  if97.specificEnthalpyPX(args[0],args[1]);
            }

        };
         net.objecthunter.exp4j.function.Function enthalpy_tsat = new net.objecthunter.exp4j.function.Function("enthalpy_tsat", 2) {
            @Override
            public double apply(double... args) {
                // specificEnthalpyPT(p;t)
                return  if97.specificEnthalpyTX(args[0],args[1]);
            }

        };
        double result = new net.objecthunter.exp4j.ExpressionBuilder("enthalpy_tsat(100, 0.2)")
                .function(enthalpy_tsat)
                .build()
                .evaluate();
        System.out.println(result);
        double expected = 3;
        //assertEquals(expected, result, 0d);
    }

    @Test
    public void testUserDefineFunction(){
        org.mariuszgromada.math.mxparser.Function g = new org.mariuszgromada.math.mxparser.Function("g"){
            @Override
            public double calculate(double... params) {
                return 2;
            }
        };
        org.mariuszgromada.math.mxparser.Expression e = new org.mariuszgromada.math.mxparser.Expression("g(x)",g);
        Argument x = new Argument("x=2");
        e.addArguments(x);
       // e.calculate();
        /*
        org.mariuszgromada.math.mxparser.Function f = new org.mariuszgromada.math.mxparser.Function("f", "x^2", "x");
        org.mariuszgromada.math.mxparser.Expression e = new org.mariuszgromada.math.mxparser.Expression("f(2)", f);

        Function g = new Function("g(x) = 2*x");
        Function f = new Function("f(x) = g(x)^2", g);
        */

        /*
        org.mariuszgromada.math.mxparser.Constant c = new org.mariuszgromada.math.mxparser.Constant("c=5");
        Argument x = new Argument("x=10/2");
        org.mariuszgromada.math.mxparser.Expression e = new org.mariuszgromada.math.mxparser.Expression("c+x");
        e.addDefinitions(x,c);
        */
        System.out.println(e.calculate());
    }
    @Test
    public void testMxParser(){
        String formular1="5^2 * 7^3 * 11^1 * 67^1 * 49201^1";
        String formular2="71^1 * 218549^1 * 6195547^1";
        org.mariuszgromada.math.mxparser.Expression eh = new org.mariuszgromada.math.mxparser.Expression(formular1);
        org.mariuszgromada.math.mxparser.Expression ew = new org.mariuszgromada.math.mxparser.Expression(formular2);
        String h = org.mariuszgromada.math.mxparser.mXparser.numberToAsciiString( eh.calculate() );
        String w = org.mariuszgromada.math.mxparser.mXparser.numberToAsciiString( ew.calculate() );
        List<FormulaM> formulas =new ArrayList<FormulaM>(2);
        FormulaM formula1=new FormulaM();
        formula1.setKey("1");
        formula1.setValue(formular1);

        FormulaM formula2=new FormulaM();
        formula2.setKey("2");
        formula2.setValue(formular2);

        formulas.add(formula1);
        formulas.add(formula2);
        List<FormulaM> formula_result=calculationService.calculate(formulas);

        System.out.println(formula_result.get(0).getResult()+" y "+formula_result.get(1).getResult());
        System.out.println(eh.calculate()+" x "+ew.calculate());
        //org.mariuszgromada.math.mxparser.mXparser.consolePrintln(h + " " + w);
    }
    @Test
    public void testServiceReg(){
        //String pattern="(U[0-9]{1,2})(D[0-9]{1,4})";
        PatternM pattern=new PatternM();
        pattern.setValue("sin(U04D122+U04D123*5)");
        pattern.setPattern("(U[0-9]{1,2})(D[0-9]{1,4})");
        pattern=calculationService.listPattern(pattern);
        Gson gson=new Gson();

        System.out.println( gson.toJson(pattern.getDataUnits()));
    }
    @Test
    public void testRegular(){
        //String pattern="(U[0-9]{1,2})(D[0-9]{1,4})";
        String pattern="(U[0-9]{1,2})(D[0-9]{1,4})";
        String fomula="sin(U04D22+U04D123*5)-U05D2";
        // Create a Pattern object
        java.util.regex.Pattern r = java.util.regex.Pattern.compile(pattern);

        // Now create matcher object.
        java.util.regex.Matcher m = r.matcher(fomula);
        /*
        if (m.find( )) {
            System.out.println("Found value: " + m.group(0) );
            System.out.println("Found value: " + m.group(1) );
            System.out.println("Found value: " + m.group(2) );
        } else {
            System.out.println("NO MATCH");
        }
        */
        int count = 0;
        while(m.find()) {
            System.out.println(m.group());
            System.out.println("Match number "+count+", value["+ m.group(2)+"]");
            count++;
            System.out.println("start(): "+m.start());
            System.out.println("end(): "+m.end());
        }

        String INPUT="";
        String REGEX="";
        String REPLACE="";
        java.util.regex.Pattern p = java.util.regex.Pattern.compile(REGEX);
        // get a matcher object
         m = p.matcher(INPUT);
        INPUT = m.replaceAll(REPLACE);
        System.out.println(INPUT);
    }
    @Test
    public void  unpackNStar () {
        // first, wrap the input array in a ByteBuffer:
        byte[] bytes = null;
        try {
            bytes = IOUtils.toByteArray(new FileInputStream(new File("/Users/imake/Desktop/AIS/data/MM08/0820140520/08201405200000.dat")));
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("file size="+bytes.length);
        ByteBuffer byteBuf = ByteBuffer.wrap( bytes );

        // then turn it into an IntBuffer, using big-endian ("Network") byte order:
        byteBuf.order( ByteOrder.BIG_ENDIAN);
        //IntBuffer intBuf = byteBuf.asIntBuffer();
       DoubleBuffer intBuf = byteBuf.asDoubleBuffer();
        //CharBuffer intBuf = byteBuf.asCharBuffer();
        /*
        [1]1.76249256904971E-309
[2]-4096.0088656031185
[3]9.208594432325948E10
[4]7.30550185897293E-309
[5]4.979435768241198E182

[1]1.0842688084497543E-19
[2]2.7230840669897744E251
[3]1.9869288110304765E218
[4]2.0843562814352433E-289
[5]3.3264264721034873E-111
         */
        //intBuf.
        // finally, dump the contents of the IntBuffer into an array
        //int[] integers = new int[ intBuf.remaining() ];
        double[] integers = new double[ intBuf.remaining() ];
        //char[] integers = new char[ intBuf.remaining() ];
        intBuf.get( integers );
        if(integers!=null){
            for (int i=0;i<integers.length;i++)
                System.out.println("["+(i+1)+"]"+integers[i]);
        }

    }
}
